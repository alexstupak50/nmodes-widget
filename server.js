require('dotenv').config();
const fs = require('fs');
const express = require('express');        // call express
const app = express();                 // define our app using express
const cors = require('cors');
// const request = require('request');
const axios = require('axios');

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', './public');


const port = process.env.PORT;

const corsOptions = {
  origin: function (origin, callback) {
    if (true) {
      //this is not tested, just copied from npm cors docs
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
};

app.use(cors(corsOptions));

app.use('/static', express.static('./public'));


// That is what users put to their sites
app.get('/v1/widget/', (req, res) => {
  const apiKey = req.query.api_key;
  fs.readFile('./public/nmodes-widget-iframe.js', 'utf8', (err, text) => {
    const textWithApiKey = text.replace(process.env.IFRAME_URL, `${process.env.IFRAME_URL}?api_key=${apiKey}`);
    res.send(textWithApiKey);
  });
});



// Path to budle in production
app.get('/widget/', (req, res) => {
  fs.readFile('./dist/nmodes-widget.js', 'utf8', (err, text) => {
    res.send(text);
  });
});


const getChatInfo = (apiKey) => {
  return axios.get(`${process.env.DASHBOARD_API}/api/v1/info/`, {headers: { Authorization: apiKey }}).then((response) => {
      if (response.status === 200) {
          return response.data;
      }
  });
};


app.get('/', async (req, res) => {
  const apiKey = req.query.api_key;
  if (!apiKey) {
    return res.send('Api key is required');
  }
  const data = await getChatInfo(apiKey);
  if (!data.active) {
    return res.send('Subscription error');
  }
  res.render('index_prod.html', { api_key: req.query.api_key, theme: data.theme });
});


app.set('port', port || 3000);
app.listen(port, () => {
  const portNum = app.get('port');
  console.log('Running on port: %s', portNum);
});






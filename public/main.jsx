import React from 'react';
import { render } from 'react-dom';
// import Example from './components/Example';
import Example from '../lib/appWithTheme';

const root = document.getElementById('root');

render(
  <Example {...(root.dataset)} />,
  root,
);

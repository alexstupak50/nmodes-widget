import './style.css';


function createIframe() {
  const iframeContainer = document.createElement('div');
  iframeContainer.id = 'nmodes-widget-container';
  const iframe = document.createElement('iframe');
  iframe.src = process.env.IFRAME_URL;
  iframe.id = 'nmodes-widget';
  iframe.allow = 'microphone';
  iframeContainer.appendChild(iframe);
  document.body.appendChild(iframeContainer);
}


function init() {
  createIframe();
  const iframe = document.getElementById('nmodes-widget');
  const iframeContainer = document.getElementById('nmodes-widget-container');
  const bodyTag = document.getElementsByTagName('body')[0];
  const defaultBodyPosition = bodyTag.style.position;
  let parentWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  let parentHeigth = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

  const onResize = function(e, opened) {

    parentWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    parentHeigth = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    iframe.contentWindow.postMessage({'width': parentWidth, 'heigth': parentHeigth}, '*');

    if (window.matchMedia('(max-width: 560px)').matches) {
      iframe.style.width = parentWidth + 'px';
    } else {
      iframe.style.width = '400px';
    }
    if (!e && !opened) {
      iframe.style.width = '76px';
      iframe.style.height = '76px';
    }
    iframeContainer.style.height = iframe.style.height;
    iframeContainer.style.width = iframe.style.width;
  };


  window.addEventListener('resize', onResize);

  window.addEventListener('message', (e) => {
    const eventName = e.data[0];
    const opened = e.data[1];
    const height = e.data[2];
    const width = e.data[3];

    if (opened) {
      iframe.classList.add('open');
    } else {
      iframe.classList.remove('open');
    }

    if (parentWidth <= '568' && opened) {
      bodyTag.style.position = 'fixed';
    } else {
      bodyTag.style.position = defaultBodyPosition;
    }
    switch (eventName) {
      case 'toggleChatBot':
        if (opened && parentWidth <= '568') {
          iframe.style.height = '100%';
        } else {
          iframe.style.height = height;
        }
        onResize(undefined, opened);
        break;
      default:
        break;
    }
  }, false);
}

init();

import React from 'react';
import { ThemeProvider } from 'styled-components';
// import ChatBot from '../../lib/index';
// import Example from '../lib/appWithTheme';

const defaultTheme = {
  background: '#FFFFFF',
  fontFamily: 'WhitneyMedium',
  headerBgColor: '#033745',
  headerFontColor: '#fff',
  headerFontSize: '16px',
  botBubbleColor: '#F3F5F9',
  botFontColor: '#666666',
  userBubbleColor: '#A7D54F',
  userFontColor: '#FFFFFF',
  logoBackgroundColor: '#F3F5F9',
  buttonTextColor: "#4ab8d9",
};


const grey = { ...defaultTheme, headerBgColor: '#555555', userBubbleColor: '#a7d745' };
const dark = { ...defaultTheme, headerBgColor: '#000000', userBubbleColor: '#666666' };

const themes = {
  blue: defaultTheme,
  default: defaultTheme,
  grey,
  dark,
};


const ThemedExample = ({ api_key, theme, floating }) => {
  const selectedTheme = themes[theme] || defaultTheme;
  console.log(selectedTheme);
  return (
    <ThemeProvider theme={selectedTheme}>
      <ChatBot floating={floating || true} apiKey={api_key} />
    </ThemeProvider>
  );
};


export default ThemedExample;

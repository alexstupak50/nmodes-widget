import React from 'react';

const FileUploadIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="19.9" height="19.2" viewBox="0 0 19.9 19.2">
    <g fill="currentColor" fillRule="nonzero">
      <path d="M17.1,19.2H2.9c-1.6,0-2.9-1.3-2.9-2.9v-2.7h1.7v2.7c0,0.6,0.5,1.1,1.1,1.1h14.2c0.6,0,1.1-0.5,1.1-1.1v-2.7 h1.7v2.7C19.9,17.9,18.6,19.2,17.1,19.2z"
      />
      <path d="M16.8,6.6l-6.4-6.4c-0.2-0.2-0.6-0.2-0.8,0L3.2,6.6C2.9,6.9,3.1,7.5,3.6,7.5h2.9v6.2c0,0.3,0.2,0.6,0.6,0.6h6 c0.3,0,0.6-0.2,0.6-0.6V7.5h2.9C16.9,7.5,17.1,6.9,16.8,6.6z"
      />
    </g>
  </svg>
);

export default FileUploadIcon;





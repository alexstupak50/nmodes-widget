import React from 'react';
import PropTypes from 'prop-types';

const SubmitIcon = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width="20.9" height="18.9" viewBox="0 0 20.9 18.9">
    <g fill="currentColor" fillRule="nonzero">
      <path width={props.size} height={props.size} d="M0,18.2v-6.6c0-0.3,0.3-0.6,0.6-0.7L14,9.4L0.6,7.9C0.3,7.9,0,7.6,0,7.2V0.7c0-0.5,0.5-0.8,0.9-0.6l19.6,8.7	c0.5,0.2,0.5,1,0,1.2L0.9,18.8C0.5,19,0,18.7,0,18.2z"/>
    </g>
  </svg>
);

SubmitIcon.propTypes = {
  size: PropTypes.number,
};

SubmitIcon.defaultProps = {
  size: 20,
};

export default SubmitIcon;

import ChatIcon from './ChatIcon';
import CloseIcon from './CloseIcon';
import SubmitIcon from './SubmitIcon';
import MicIcon from './MicIcon';
import PoweredByIcon from './PoweredByIcon';
import AttachementIcon from './AttachementIcon';
import FileUploadIcon from './FileUploadIcon';
import ThumbUpIcon from './ThumbUpIcon';
import ThumbDownIcon from './ThumbDownIcon';
import SmileIcon from './SmileIcon';

export {
  ChatIcon,
  CloseIcon,
  SubmitIcon,
  MicIcon,
  PoweredByIcon,
  AttachementIcon,
  FileUploadIcon,
  ThumbUpIcon,
  ThumbDownIcon,
  SmileIcon,
};

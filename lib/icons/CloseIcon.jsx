import React from 'react';

const CloseIcon = ({width = '20.2', height = '20.2'}) => (
  <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 20.2 20.2">
    <polygon points="20.2,1.1 19.1,0 10.1,9 1.1,0 0,1.1 9,10.1 0,19.1 1.1,20.2 10.1,11.1 19.1,20.2 20.2,19.1 11.1,10.1"/>
  </svg>
);


export default CloseIcon;

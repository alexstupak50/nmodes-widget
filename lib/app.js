import React from 'react';
import { render } from 'react-dom';
import ThemedExample from './appWithTheme';

const root = document.getElementById('nmodes-widget');

render(
  <ThemedExample {...(root.dataset)} />,
  root,
);

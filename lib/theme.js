export default {
  background: '#FFFFFF',
  fontFamily: 'WhitneyMedium',
  headerBgColor: '#033745',
  headerFontColor: '#fff',
  headerFontSize: '16px',
  botBubbleColor: '#F3F5F9',
  botFontColor: '#666666',
  userBubbleColor: '#A7D54F',
  userFontColor: '#FFFFFF',
  logoBackgroundColor: '#F3F5F9',
};

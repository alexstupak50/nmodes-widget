import _ from 'lodash';
import axios from 'axios';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as storage from './storage';

import { CustomStep, OptionsStep, TextStep, GeneralStep, UploadingMessage, FileUpload, ImageStep } from './steps_components';
import Global from './common/GlobalStyle';
import theme from './theme';
import CreditsContext from './context';



import {
  ChatBotContainer,
  Content,
  Header,
  HeaderTitle,
  HeaderIcon,
  FloatButton,
  Footer,
  Input,
  SubmitButton,
  ActionButton,
  PoweredBy,
  VerticalBorder,
} from './components';

import Recognition from './recognition';
import { ChatIcon, CloseIcon, SubmitIcon, MicIcon, AttachementIcon, SmileIcon, ThumbDownIcon, ThumbUpIcon, FileUploadIcon } from './icons';
import { isMobile } from './utils';
import ActionContainer from "./components/Footer/ActionContainer";
import LogoContainer from "./components/LogoContainer";
import PoweredByIcon from "./icons/PoweredByIcon";
import FooterBootom from "./components/FooterBottom";



class ChatBot extends Component {
  static contextType = CreditsContext;

  /* istanbul ignore next */
  constructor(props) {
    super(props);

    this.state = {
      renderedSteps: [],
      previousSteps: [],
      currentStep: {},
      previousStep: {},
      steps: {},
      uploadingFile: false,
      disabled: true,
      opened: props.opened || !props.floating,
      inputValue: '',
      inputInvalid: false,
      speaking: false,
      recognitionEnable: props.recognitionEnable && Recognition.isSupported(),
      defaultUserSettings: {},
    };

    this.renderStep = this.renderStep.bind(this);
    this.getTriggeredStep = this.getTriggeredStep.bind(this);
    this.generateRenderedStepsById = this.generateRenderedStepsById.bind(this);
    this.triggerNextStep = this.triggerNextStep.bind(this);
    this.onResize = this.onResize.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
    this.onRecognitionChange = this.onRecognitionChange.bind(this);
    this.onRecognitionEnd = this.onRecognitionEnd.bind(this);
    this.onRecognitionStop = this.onRecognitionStop.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleSubmitButton = this.handleSubmitButton.bind(this);
  }

  async loadFirstStep() {
    const steps = {};
    const {
      userAvatar,
      userDelay,
    } = this.props;
    const defaultUserSettings = { delay: userDelay, avatar: userAvatar };
    let firstStep = await this.getStep('Get started');
    firstStep = firstStep[0];
    const renderedSteps = [];
    const previousSteps = [];
    const previousStep = {};

    const currentStep = this.setFlow(firstStep, renderedSteps, previousSteps);

    this.setState({
      currentStep,
      defaultUserSettings,
      previousStep,
      previousSteps,
      renderedSteps,
      steps,
    });
  }

  async componentWillMount() {
    if (!this.props.floating && this.state.opened) {
      await this.loadFirstStep();
    }
  }

  setFlow(step, renderedSteps, previousSteps) {
    const options = step.options;
    delete step.options;
    step.user = false;
    let currentStep = step;
    renderedSteps.push(step);
    previousSteps.push(step);

    const optionStep = {
      id: 'options',
      options,
      user: false,
    };

    if (optionStep.options) {
      currentStep = optionStep;
      renderedSteps.push(currentStep);
      previousSteps.push(currentStep);
    }
    return currentStep;
  }


  bindEvent(element, eventName, eventHandler) {
    if (element.addEventListener) {
      element.addEventListener(eventName, eventHandler, false);
    } else if (element.attachEvent) {
      element.attachEvent('on' + eventName, eventHandler);
    }
  }

  componentDidMount() {
    const { recognitionEnable } = this.state;
    const { recognitionLang } = this.props;
    if (recognitionEnable) {
      this.recognition = new Recognition(
        this.onRecognitionChange,
        this.onRecognitionEnd,
        this.onRecognitionStop,
        recognitionLang,
      );
    }
    this.content.addEventListener('DOMNodeInserted', this.onNodeInserted);
    window.addEventListener('resize', this.onResize);
    const self = this;
    this.bindEvent(window, 'message', (e) => {
      self.parentWidth = e.data.width;
    });
  }

  componentWillUpdate(nextProps, nextState) {
    const { opened } = nextProps;

    if (opened !== undefined && opened !== nextState.opened) {
      this.setState({ opened });
    }
  }

  componentWillUnmount() {
    this.content.removeEventListener('DOMNodeInserted', this.onNodeInserted);
    window.removeEventListener('resize', this.onResize);
  }

  onNodeInserted(event) {
    event.currentTarget.scrollTop = event.currentTarget.scrollHeight;
  }

  onResize() {
    this.content.scrollTop = this.content.scrollHeight;
  }

  onRecognitionChange(value) {
    this.setState({ inputValue: value });
  }

  onRecognitionEnd() {
    this.setState({ speaking: false });
    this.handleSubmitButton();
  }

  onRecognitionStop() {
    this.setState({ speaking: false });
  }

  onValueChange(event) {
    this.setState({ inputValue: event.target.value });
  }

  getTriggeredStep(trigger, value) {
    const steps = this.generateRenderedStepsById();
    return typeof trigger === 'function' ? trigger({ value, steps }) : trigger;
  }

  getStepMessage(message) {
    const { previousSteps } = this.state;
    const lastStepIndex = previousSteps.length > 0 ? previousSteps.length - 1 : 0;
    const steps = this.generateRenderedStepsById();
    const previousValue = previousSteps[lastStepIndex].value;
    return typeof message === 'function' ? message({ previousValue, steps }) : message;
  }

  generateRenderedStepsById() {
    const { previousSteps } = this.state;
    const steps = {};

    for (let i = 0, len = previousSteps.length; i < len; i += 1) {
      const { id, message, value, metadata } = previousSteps[i];
      steps[id] = { id, message, value, metadata };
    }

    return steps;
  }

  async triggerNextStep(data, currentStep) {
    const { enableMobileAutoFocus } = this.props;
    const {
      defaultUserSettings,
      previousSteps,
      renderedSteps,
      steps,
    } = this.state;

    let { previousStep } = this.state;
    currentStep = currentStep || this.state.currentStep;

    const isEnd = currentStep.end;

    if (data && data.value) {
      currentStep.value = data.value;
    }

    if (data && data.typing) {
      const typingStep = {
        user: false,
        message: '...',
        delay: 100,
        typing: true,
        avatar: this.props.botAvatar,
      };

      renderedSteps.push(typingStep);
      previousSteps.push(typingStep);

      this.setState({
        currentStep: typingStep,
        renderedSteps,
        previousSteps,
      });
      return;
    }

    if (data && data.hideInput) {
      currentStep.hideInput = data.hideInput;
    }

    if (data && data.trigger) {
      delete currentStep.options;
      currentStep.trigger = data.trigger;
      // currentStep.trigger = this.getTriggeredStep(data.trigger, data.value);
    }

    if (isEnd) {
      this.handleEnd();
    } else if (currentStep.options && data) {
      const option = currentStep.options.filter(o => o.value === data.value)[0];
      const trigger = this.getTriggeredStep(option.trigger, currentStep.value);
      delete currentStep.options;

      // replace choose option for user message
      currentStep = Object.assign({}, currentStep, option, defaultUserSettings, {
        user: true,
        message: option.label,
        trigger,
        delay: 1,
      });


      renderedSteps.pop();
      previousSteps.pop();
      renderedSteps.push(currentStep);
      previousSteps.push(currentStep);

      this.setState({
        currentStep,
        renderedSteps,
        previousSteps,
      });
    } else if (currentStep.trigger) {
      if (currentStep.replace) {
        renderedSteps.pop();
      }
      let nextStep;

      if (!currentStep.trigger) {
        return;
      }

      if (!renderedSteps.some(step => step.typing)) {
        this.triggerNextStep({typing: true});
      }

      const nextSteps = await this.getStep(currentStep.trigger);

      let lastStep = renderedSteps[renderedSteps.length-1];

      if (lastStep.typing) {
        renderedSteps.pop();
        previousSteps.pop();
      }

      let apiStep;
      for (apiStep of nextSteps) {
        apiStep = this.setFlow(apiStep, renderedSteps, previousSteps);
      }

      previousStep = apiStep;
      currentStep = apiStep;
      nextStep = currentStep;


      this.setState({ renderedSteps, currentStep, previousStep }, () => {
        if (nextStep.user) {
          this.setState({ disabled: false }, () => {
            if (enableMobileAutoFocus || !isMobile()) {
              this.input.focus();
            }
          });
        } else {
          this.setState({ renderedSteps, previousSteps });
        }
      });
    }

    const { cache, cacheName } = this.props;
    if (cache) {
      setTimeout(() => {
        storage.setData(cacheName, {
          currentStep,
          previousStep,
          previousSteps,
          renderedSteps,
        });
      }, 300);
    }
  }

  handleEnd() {
    if (this.props.handleEnd) {
      const { previousSteps } = this.state;

      const renderedSteps = previousSteps.map((step) => {
        const { id, message, value, metadata } = step;
        return { id, message, value, metadata };
      });

      const steps = [];

      for (let i = 0, len = previousSteps.length; i < len; i += 1) {
        const { id, message, value, metadata } = previousSteps[i];
        steps[id] = { id, message, value, metadata };
      }

      const values = previousSteps.filter(step => step.value).map(step => step.value);

      this.props.handleEnd({ renderedSteps, steps, values });
    }
  }

  isLastPosition(step) {
    const { renderedSteps } = this.state;
    const length = renderedSteps.length;
    const stepIndex = renderedSteps.map(s => s.key).indexOf(step.key);

    if (length <= 1 || stepIndex + 1 === length) {
      return true;
    }

    const nextStep = renderedSteps[stepIndex + 1];
    const hasMessage = nextStep.message || nextStep.asMessage;

    if (!hasMessage) {
      return true;
    }

    const isLast = step.user !== nextStep.user;
    return isLast;
  }

  isFirstPosition(step) {
    const { renderedSteps } = this.state;
    const stepIndex = renderedSteps.map(s => s.key).indexOf(step.key);

    if (stepIndex === 0) {
      return true;
    }

    const lastStep = renderedSteps[stepIndex - 1];
    const hasMessage = lastStep.message || lastStep.asMessage;

    if (!hasMessage) {
      return true;
    }

    const isFirst = step.user !== lastStep.user;
    return isFirst;
  }

  handleKeyPress(event) {
    if (event.key === 'Enter') {
      this.submitUserMessage();
    }
  }

  async handleSubmitButton() {
    const { inputValue, speaking, recognitionEnable } = this.state;
    if ((_.isEmpty(inputValue) || speaking) && recognitionEnable) {
      this.recognition.speak();
      if (!speaking) {
        this.setState({ speaking: true });
      }
      return;
    }
    await this.submitUserMessage();
  }

  async submitUserMessage() {
    const { defaultUserSettings, inputValue, previousSteps, renderedSteps } = this.state;
    let { currentStep } = this.state;

    const isInvalid = currentStep.validator && this.checkInvalidInput();

    if (!isInvalid) {


      const step = {
        message: inputValue,
        value: inputValue,
        user: true,
      };

      currentStep = step;
      // currentStep = Object.assign({}, defaultUserSettings, step);

      renderedSteps.push(currentStep);
      previousSteps.push(currentStep);

      this.setState({ currentStep, renderedSteps, previousSteps });
      const newState = {
        renderedSteps,
        previousSteps,
        disabled: true,
        inputValue: '',
      };

      this.setState(newState, () => {
        this.input.blur();
        this.input.focus();
      });
      this.triggerNextStep({ trigger: inputValue }, currentStep);
    }
  }

  checkInvalidInput() {
    const { enableMobileAutoFocus } = this.props;
    const { currentStep, inputValue } = this.state;
    const result = currentStep.validator(inputValue);
    const value = inputValue;

    if (typeof result !== 'boolean' || !result) {
      this.setState({
        inputValue: result.toString(),
        inputInvalid: true,
        disabled: true,
      }, () => {
        setTimeout(() => {
          this.setState({
            inputValue: value,
            inputInvalid: false,
            disabled: false,
          }, () => {
            if (enableMobileAutoFocus || !isMobile()) {
              this.input.focus();
            }
          });
        }, 2000);
      });

      return true;
    }

    return false;
  }

  async toggleChatBot(opened){

    // console.log("info");
    // console.log(parent.document.body.clientHeight);
    // console.log(parent.document.body.clientWidth);

    let height = '100px';
    let width = '100px';

    if (opened) {
      height = '700px';
      width = '380px';
    }

    window.parent.postMessage(['toggleChatBot', opened, height, width], '*');

    if (this.state.renderedSteps.length === 0) {
      await this.loadFirstStep();
    }

    if (this.props.toggleFloating) {
      this.props.toggleFloating({ opened });
    } else {
      this.setState({ opened });
    }
  }

  getApiUrl() {
    return this.context.apiUrl || process.env.API_URL;
  }

  async getStep(text = '') {
    const res = await axios(
      `${this.getApiUrl()}?text=${text}`, { withCredentials: 'include', headers: { Authorization: `${this.context.apiKey}`} });
    return res.data;
  }

  uploadDocumentChangeStatus = (status) => {
    this.setState({ uploadingFile: status });
  };

  stopUploading = () => {
    this.uploadDocumentChangeStatus(false);
  };

  addStep = (step) => {
    const { renderedSteps, previousSteps } = this.state;
    renderedSteps.push(step);
    previousSteps.push(step);
    this.setState({ currentStep: step, renderedSteps, previousSteps });
  };

  renderStep(step, index) {
    const { renderedSteps } = this.state;
    const {
      avatarStyle,
      bubbleStyle,
      bubbleOptionStyle,
      customStyle,
      hideBotAvatar,
      hideUserAvatar,
    } = this.props;
    const { options, component, asMessage } = step;
    const steps = this.generateRenderedStepsById();
    const previousStep = index > 0 ? renderedSteps[index - 1] : {};

    if (step.type === 'file') {
      return (
        <ImageStep
          key={index}
          step={step}
          steps={steps}
          previousStep={previousStep}
          triggerNextStep={this.triggerNextStep}
        />
      );
    }

    if (step.type === 'general') {
      return (
        <GeneralStep
          key={index}
          step={step}
          steps={steps}
          previousStep={previousStep}
          triggerNextStep={this.triggerNextStep}
        />
      );
    }

    if (component && !asMessage) {
      return (
        <CustomStep
          key={index}
          step={step}
          steps={steps}
          style={customStyle}
          previousStep={previousStep}
          triggerNextStep={this.triggerNextStep}
        />
      );
    }

    if (options) {
      return (
        <OptionsStep
          key={index}
          step={step}
          triggerNextStep={this.triggerNextStep}
          bubbleOptionStyle={bubbleOptionStyle}
        />
      );
    }

    return (
      <TextStep
        key={index}
        step={step}
        steps={steps}
        previousStep={previousStep}
        previousValue={previousStep.value}
        triggerNextStep={this.triggerNextStep}
        avatarStyle={avatarStyle}
        bubbleStyle={bubbleStyle}
        hideBotAvatar={hideBotAvatar}
        hideUserAvatar={hideUserAvatar}
        isFirst={this.isFirstPosition(step)}
        isLast={this.isLastPosition(step)}
      />
    );
  }

  render() {
    let {
      currentStep,
      disabled,
      inputInvalid,
      inputValue,
      opened,
      renderedSteps,
      speaking,
      recognitionEnable,
    } = this.state;
    const {
      className,
      contentStyle,
      floating,
      floatingStyle,
      footerStyle,
      headerComponent,
      headerTitle,
      hideHeader,
      hideSubmitButton,
      inputStyle,
      placeholder,
      inputAttributes,
      recognitionPlaceholder,
      style,
      submitButtonStyle,
      width,
      height,
      footerHeight,
    } = this.props;

    const header = headerComponent || (
      <Header className="rsc-header">
        <HeaderTitle className="rsc-header-title"><PoweredByIcon style={{paddingTop: "6px"}} height={25} width={113} colorStyle="white"/></HeaderTitle>
        {floating && (
          <HeaderIcon className="rsc-header-close-button" onClick={() => this.toggleChatBot(false)}>
            <CloseIcon />
          </HeaderIcon>
        )}
      </Header>
    );

    const icon =
      (_.isEmpty(inputValue) || speaking) && recognitionEnable ? <MicIcon /> : <SubmitIcon />;

    const inputPlaceholder = speaking ? recognitionPlaceholder :
      currentStep.placeholder || placeholder;

    const inputAttributesOverride = currentStep.inputAttributes || inputAttributes;
    currentStep.hideInput = false;
    disabled = false;
    return (
        <Global theme={theme}>
          <div className={`rsc ${className}`}>
            {floating && (
              <FloatButton
                className="rsc-float-button"
                style={floatingStyle}
                opened={opened}
                onClick={() => this.toggleChatBot(true)}
              >
                <ChatIcon />
              </FloatButton>
            )}
            <ChatBotContainer
              className="rsc-container"
              floating={floating}
              floatingStyle={floatingStyle}
              opened={opened}
              style={style}
              width={width}
              height={height}
              parentWidth={this.parentWidth}
            >
              {!hideHeader && header}
              <Content
                className="rsc-content"
                innerRef={contentRef => (this.content = contentRef)}
                floating={floating}
                style={contentStyle}
                height={height}
                hideInput={currentStep.hideInput}
                footerHeight={footerHeight}
              >
                {_.map(renderedSteps, this.renderStep)}

              </Content>

              <Footer className="rsc-footer" style={footerStyle} footerHeight={footerHeight}>
                { this.state.uploadingFile && <UploadingMessage stopUploading={this.stopUploading} /> }
                {!currentStep.hideInput && (
                  <Input
                    type="textarea"
                    style={inputStyle}
                    innerRef={inputRef => (this.input = inputRef)}
                    className="rsc-input"
                    placeholder={inputInvalid ? '' : inputPlaceholder}
                    onKeyPress={this.handleKeyPress}
                    onChange={this.onValueChange}
                    value={inputValue}
                    floating={floating}
                    invalid={inputInvalid}
                    disabled={disabled}
                    hasButton={!hideSubmitButton}
                    {...inputAttributesOverride}
                  />
                )}
                {!currentStep.hideInput && !hideSubmitButton && (
                  <SubmitButton
                    className="rsc-submit-button"
                    style={submitButtonStyle}
                    onClick={this.handleSubmitButton}
                    invalid={inputInvalid}
                    disabled={disabled}
                    speaking={speaking}
                  >
                    {icon}
                  </SubmitButton>
                )}

                <FooterBootom>
                  <ActionContainer>
                    <FileUpload uploadDocumentChangeStatus={this.uploadDocumentChangeStatus} addStep={this.addStep}/>
                    {/*<ActionButton><SmileIcon /></ActionButton>*/}
                    {/*<VerticalBorder />*/}
                    {/*<ActionButton><ThumbUpIcon /></ActionButton>*/}
                    {/*<ActionButton><ThumbDownIcon /></ActionButton>*/}
                    {/*<VerticalBorder />*/}
                    {/*<ActionButton><FileUploadIcon /></ActionButton>*/}
                  </ActionContainer>

                  <PoweredBy />
                </FooterBootom>
              </Footer>

            </ChatBotContainer>
          </div>
        </Global>
    );
  }
}


ChatBot.propTypes = {
  avatarStyle: PropTypes.object,
  botAvatar: PropTypes.string,
  botDelay: PropTypes.number,
  bubbleOptionStyle: PropTypes.object,
  bubbleStyle: PropTypes.object,
  cache: PropTypes.bool,
  cacheName: PropTypes.string,
  className: PropTypes.string,
  contentStyle: PropTypes.object,
  customDelay: PropTypes.number,
  customStyle: PropTypes.object,
  enableMobileAutoFocus: PropTypes.bool,
  floating: PropTypes.bool,
  floatingStyle: PropTypes.object,
  footerStyle: PropTypes.object,
  handleEnd: PropTypes.func,
  headerComponent: PropTypes.element,
  headerTitle: PropTypes.string,
  hideBotAvatar: PropTypes.bool,
  hideHeader: PropTypes.bool,
  hideSubmitButton: PropTypes.bool,
  hideUserAvatar: PropTypes.bool,
  inputAttributes: PropTypes.object,
  inputStyle: PropTypes.object,
  opened: PropTypes.bool,
  toggleFloating: PropTypes.func,
  placeholder: PropTypes.string,
  recognitionEnable: PropTypes.bool,
  recognitionLang: PropTypes.string,
  recognitionPlaceholder: PropTypes.string,
  style: PropTypes.object,
  submitButtonStyle: PropTypes.object,
  userAvatar: PropTypes.string,
  userDelay: PropTypes.number,
  width: PropTypes.string,
  height: PropTypes.string,
};

ChatBot.defaultProps = {
  avatarStyle: {},
  botDelay: 100,
  bubbleOptionStyle: {},
  bubbleStyle: {},
  cache: false,
  cacheName: 'rsc_cache',
  className: 'nmodes_widget',
  contentStyle: {},
  customStyle: {},
  customDelay: 100,
  enableMobileAutoFocus: false,
  floating: false,
  floatingStyle: {},
  footerStyle: {},
  handleEnd: undefined,
  headerComponent: undefined,
  headerTitle: '',
  hideBotAvatar: false,
  hideHeader: false,
  hideSubmitButton: false,
  hideUserAvatar: true,
  inputStyle: {},
  opened: undefined,
  placeholder: 'Type a message here',
  inputAttributes: {},
  recognitionEnable: false,
  recognitionLang: 'en',
  recognitionPlaceholder: 'Listening ...',
  style: {},
  submitButtonStyle: {},
  toggleFloating: undefined,
  userDelay: 100,
  width: '350px',
  height: '700px',
  footerHeight: '112px',
  botAvatar:
    'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyMy43MiAyNS45NyI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiMzNzZmN2E7fS5jbHMtMntmaWxsOiMyOThiYzA7fTwvc3R5bGU+PC9kZWZzPjx0aXRsZT5Bc3NldCAzPC90aXRsZT48ZyBpZD0iTGF5ZXJfMiIgZGF0YS1uYW1lPSJMYXllciAyIj48ZyBpZD0ibmV3X2Rlc2lnbiIgZGF0YS1uYW1lPSJuZXcgZGVzaWduIj48cGF0aCBjbGFzcz0iY2xzLTEiIGQ9Ik01LjA2LDE0LjkyQTkuNDQsOS40NCwwLDEsMCwxNC44LDMuNDgsOC40Niw4LjQ2LDAsMSwxLDUuMDYsMTQuOTJaIi8+PHBhdGggY2xhc3M9ImNscy0yIiBkPSJNMjIuNDQsNi43MUExMS44LDExLjgsMCwxLDAsOC42NSwyMy4xNmwuMTcuMDUtLjEuMTVBNy4yNCw3LjI0LDAsMCwxLDUuMTQsMjZhMjEuOTQsMjEuOTQsMCwwLDAsOS40NC0yLjcxaDBsLjUxLS4xNGE4LjA2LDguMDYsMCwwLDEtLjg1LDBBMTAuMjUsMTAuMjUsMCwxLDEsMjIuNDQsNi43MVoiLz48L2c+PC9nPjwvc3ZnPg==',
  userAvatar:
    'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgLTIwOC41IDIxIDEwMCAxMDAiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9Ii0yMDguNSAyMSAxMDAgMTAwIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PGc+PGNpcmNsZSBjeD0iLTE1OC41IiBjeT0iNzEiIGZpbGw9IiNGNUVFRTUiIGlkPSJNYXNrIiByPSI1MCIvPjxnPjxkZWZzPjxjaXJjbGUgY3g9Ii0xNTguNSIgY3k9IjcxIiBpZD0iTWFza18yXyIgcj0iNTAiLz48L2RlZnM+PGNsaXBQYXRoIGlkPSJNYXNrXzRfIj48dXNlIG92ZXJmbG93PSJ2aXNpYmxlIiB4bGluazpocmVmPSIjTWFza18yXyIvPjwvY2xpcFBhdGg+PHBhdGggY2xpcC1wYXRoPSJ1cmwoI01hc2tfNF8pIiBkPSJNLTEwOC41LDEyMXYtMTRjMCwwLTIxLjItNC45LTI4LTYuN2MtMi41LTAuNy03LTMuMy03LTEyICAgICBjMC0xLjcsMC02LjMsMC02LjNoLTE1aC0xNWMwLDAsMCw0LjYsMCw2LjNjMCw4LjctNC41LDExLjMtNywxMmMtNi44LDEuOS0yOC4xLDcuMy0yOC4xLDYuN3YxNGg1MC4xSC0xMDguNXoiIGZpbGw9IiNFNkMxOUMiIGlkPSJNYXNrXzNfIi8+PGcgY2xpcC1wYXRoPSJ1cmwoI01hc2tfNF8pIj48ZGVmcz48cGF0aCBkPSJNLTEwOC41LDEyMXYtMTRjMCwwLTIxLjItNC45LTI4LTYuN2MtMi41LTAuNy03LTMuMy03LTEyYzAtMS43LDAtNi4zLDAtNi4zaC0xNWgtMTVjMCwwLDAsNC42LDAsNi4zICAgICAgIGMwLDguNy00LjUsMTEuMy03LDEyYy02LjgsMS45LTI4LjEsNy4zLTI4LjEsNi43djE0aDUwLjFILTEwOC41eiIgaWQ9Ik1hc2tfMV8iLz48L2RlZnM+PGNsaXBQYXRoIGlkPSJNYXNrXzVfIj48dXNlIG92ZXJmbG93PSJ2aXNpYmxlIiB4bGluazpocmVmPSIjTWFza18xXyIvPjwvY2xpcFBhdGg+PHBhdGggY2xpcC1wYXRoPSJ1cmwoI01hc2tfNV8pIiBkPSJNLTE1OC41LDEwMC4xYzEyLjcsMCwyMy0xOC42LDIzLTM0LjQgICAgICBjMC0xNi4yLTEwLjMtMjQuNy0yMy0yNC43cy0yMyw4LjUtMjMsMjQuN0MtMTgxLjUsODEuNS0xNzEuMiwxMDAuMS0xNTguNSwxMDAuMXoiIGZpbGw9IiNENEIwOEMiIGlkPSJoZWFkLXNoYWRvdyIvPjwvZz48L2c+PHBhdGggZD0iTS0xNTguNSw5NmMxMi43LDAsMjMtMTYuMywyMy0zMWMwLTE1LjEtMTAuMy0yMy0yMy0yM3MtMjMsNy45LTIzLDIzICAgIEMtMTgxLjUsNzkuNy0xNzEuMiw5Ni0xNTguNSw5NnoiIGZpbGw9IiNGMkNFQTUiIGlkPSJoZWFkIi8+PC9nPjwvc3ZnPg==',
};

export default ChatBot;

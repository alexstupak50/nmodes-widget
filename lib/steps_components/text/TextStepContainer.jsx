import styled from 'styled-components';

const TextStepContainer = styled.div`
  align-items: flex-end;
  display: flex;
  justify-content: ${props => props.user ? 'flex-end' : 'flex-start'};
  margin: 0 20px;
`;

export default TextStepContainer;

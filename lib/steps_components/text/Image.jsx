import styled from 'styled-components';
import { scale } from '../../common/animations';

const Image = styled.img`
  animation: ${scale} .3s ease forwards;
  height: 30px;
  min-width: 40px;
  padding: 3px;
  width: 40px;
`;

export default Image;

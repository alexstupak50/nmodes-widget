import styled from 'styled-components';
import TextStepContainer from '../text/TextStepContainer';


const ImageStepContainer = styled(TextStepContainer)`
  justify-content: flex-end;
  margin-top: 10px;
  margin-bottom: 10px;
`;

export default ImageStepContainer;

import styled from 'styled-components';

const Image = styled.img`
   max-width: 100%;
   max-height: 160px;
   border-radius: 10px;
`;

export default Image;

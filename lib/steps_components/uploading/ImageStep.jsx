import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Image from './Image';
import FileIcon from "../../icons/FileIcon"
import ImageStepContainer from "./ImageStepContainer";


class ImageStep extends Component {
  /* istanbul ignore next */
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
    };

    this.renderMessage = this.renderMessage.bind(this);
  }

  getFilename(url) {
    return url.split('/').pop().split('?')[0];
  }

  checkURL(url) {
    return(url.toLowerCase().match(/\.(jpeg|jpg|gif|png)$/) != null);
  }

  renderMessage() {
    const { step } = this.props;

    if (step.file && this.checkURL(step.file)) {
      return <Image src={step.file} />
    } else {
      return <div>
        <FileIcon/>
        {this.getFilename(step.file)}
      </div>
    }
  }

  render() {
    const { step } = this.props;
    const { user } = step;

    return (
      <ImageStepContainer
        className="rsc-ts"
        user={user}
      >
        {this.renderMessage()}
      </ImageStepContainer>
    );
  }
}

ImageStep.propTypes = {
  step: PropTypes.object.isRequired,
};

ImageStep.defaultProps = {
  steps: {},
};

export default ImageStep;

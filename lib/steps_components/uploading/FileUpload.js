import React, { Component } from 'react';
import axios from 'axios';

import AttachementIcon from '../../icons/AttachementIcon';
import ActionButton from '../../components/ActionButton';
import CreditsContext from '../../context';

class FileUpload extends Component {
  static contextType = CreditsContext;

  constructor(props) {
    super(props);
    this.state = {
      uploadStatus: false,
    };
    this.handleUploadImage = this.handleUploadImage.bind(this);
  }

  handleUploadImage = () => {
    const data = new FormData();
    this.props.uploadDocumentChangeStatus(true);
    let index = 0;

    for(let file of this.uploadInput.files) {
      data.append(`files[${index}]`, file);
      index++;
    }

    axios.post(this.context.uploadFilesUrl, data, {headers: { Authorization: `${this.context.apiKey}` }})
      .then((response) => {
        this.props.uploadDocumentChangeStatus(false);

        response.data.result.forEach((data)=> {
          const imageStep = {
            user: false,
            type: 'file',
            file: data.url,
            delay: 100,
          };
          this.props.addStep(imageStep);

          if(data.label) {
            const labelStep = {
              user: true,
              "id": "api",
              message: data.label,
              delay: 100,
            };
            this.props.addStep(labelStep);
          }

          if(data.translatedText) {
            const translatedTextStep = {
              user: true,
              "id": "api",
              message: data.translatedText,
              delay: 100,
            };
            this.props.addStep(translatedTextStep);
          }
        });

      })
      .catch((error) => {
        this.props.uploadDocumentChangeStatus(false);
        console.log(error);
      });
  };

  uploadDocument = () => {
    this.uploadInput.click();
  };


  commonChangeFile = (e) => {
    this.files = this.uploadInput.files;
    this.handleUploadImage(e);
  };

  render() {
    return(
      <ActionButton onClick={this.uploadDocument}>
        <input onChange={this.commonChangeFile} style={{display: "none"}} ref={(ref) => { this.uploadInput = ref; }} type='file' multiple />
        <AttachementIcon />
      </ActionButton>
    )
  }
}


export default FileUpload;

import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  padding: 10px 20px;
  justify-content: space-between;
  border-radius: 1px;
  background-color: #ebebeb;
  top: -50px;
  right: 0;
  width: 87%;
  position: absolute;  
  svg {
    cursor: pointer;
  }
`;

export default Container;

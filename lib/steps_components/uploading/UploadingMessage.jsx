import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from "../../icons/CloseIcon";
import Container from "./Container";


class UploadingMessage extends Component {
  /* istanbul ignore next */
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container>
        Uploading attachment ...
        <div onClick={this.props.stopUploading}>
          <CloseIcon width="10" height="10" />
        </div>
      </Container>
    );
  }
}

UploadingMessage.propTypes = {
  stopUploading: PropTypes.func,
};

export default UploadingMessage;

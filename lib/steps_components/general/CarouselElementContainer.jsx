import styled from 'styled-components';

const CarouselContainer = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
`;

export default CarouselContainer;

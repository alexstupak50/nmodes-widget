import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loading from '../common/Loading';
import GeneralStepContainer from './GeneralStepContainer';
import styled from 'styled-components';
import { Carousel } from 'react-responsive-carousel';
import Legend from "./Legend";
// import 'react-responsive-carousel/lib/styles/carousel.css';
import './carousel.css';
import CarouselElementContainer from "./CarouselElementContainer";
import ButtonsContainer from "./ButtonsContainer";
import Button from "./Button";
import ImageContainer from "./ImageContainer";
import {TitleContainer, TitleMain, TitleSub} from "./Titles";



const Image = styled.img`
  margin: 23px auto;
  display: block;
`;


class GeneralStep extends Component {
  /* istanbul ignore next */
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
    };

    this.renderComponent = this.renderComponent.bind(this);
    this.renderButton = this.renderButton.bind(this);
  }

  componentDidMount() {
    const { step } = this.props;
    const { delay, waitAction } = step;

    setTimeout(() => {
      this.setState({ loading: false }, () => {
        if (!waitAction && !step.rendered) {
          this.props.triggerNextStep();
        }
      });
    }, delay);
  }

  renderButton(button, index) {
    if (button.type === 'web_url') {
      return <Button onClick={()=>{window.open(button.url, '_blank')}} key={index}>{button.title}</Button>;
    }
    // postback
    return (
      <Button key={index} onClick={() => this.props.triggerNextStep({ trigger: button.payload })}>{button.title}</Button>
    );
  }


  renderElement(element, index) {

    return (
      <CarouselElementContainer key={index}>
        <ImageContainer src={element.image_url} />
        <Legend className="legend">
          <TitleContainer>
            <TitleMain>{element.title}</TitleMain>
            <TitleSub>{element.subtitle}</TitleSub>
          </TitleContainer>
          <ButtonsContainer>
            {element.buttons.map(this.renderButton)}
          </ButtonsContainer>
        </Legend>
      </CarouselElementContainer>
    );
  }

  renderComponent() {
    const { step } = this.props;
    const { elements } = step;
    const minButtonsCount = Math.max(elements.map(el => el.buttons.length));
    return (
      <Carousel showThumbs={false} showIndicators={false} showStatus={false}>
        {elements.map((element, index) => (
          this.renderElement(element, index, minButtonsCount)
        ))}
      </Carousel>
    );
  }

  render() {
    const { loading } = this.state;

    return (
      <GeneralStepContainer>
        {
          loading ? (
            <Loading />
          ) : this.renderComponent()
        }
      </GeneralStepContainer>
    );
  }
}

GeneralStep.propTypes = {
  step: PropTypes.object.isRequired,
  steps: PropTypes.object.isRequired,
  previousStep: PropTypes.object.isRequired,
  triggerNextStep: PropTypes.func.isRequired,
};

export default GeneralStep;

import styled from 'styled-components';


const Legend = styled.div`
  height: 58%;
  flex-wrap: wrap;
  display: flex;
  & > div {
    width: 100%;
  }
`;

export default Legend;

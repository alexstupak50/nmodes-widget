import styled from 'styled-components';

export const TitleContainer = styled.div`
  text-align: left;
  padding: 20px;
`;

export const TitleSub = styled.div`
  color: #696969;
  font-family: WhitneyBook;
  font-size: 12px;
`;


export const TitleMain = styled(TitleSub)`
  font-family: WhitneyMedium;
  text-transform: uppercase;
  padding-bottom: 5px;
`;







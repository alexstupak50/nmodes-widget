import styled from 'styled-components';

const ImageContainer = styled.div`
  height: 166px;
  width: 100%;
  background-image: url("${props => props.src}");
  background-size: cover;
`;

export default ImageContainer;

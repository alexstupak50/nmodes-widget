import styled from 'styled-components';

const Button = styled.div`
  display: block;
  color: ${({ theme }) => theme.buttonTextColor};
  border-top: 1px solid #dbdbdb;
  padding: 7px 0;
  font-family: WhitneyMedium;
  font-size: 14px;
  
  &:hover {
    cursor: pointer;
    background-color: #00000008;
  }
`;

export default Button;

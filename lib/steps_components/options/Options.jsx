import styled from 'styled-components';

const Options = styled.ul`
  margin: 2px 20px 12px 20px;
  padding: 0;
  text-align: center;
`;

export default Options;

import styled from 'styled-components';
import { scale } from '../../common/animations';

const Option = styled.li`
  animation: ${scale} .3s ease forwards;
  cursor: pointer;
  display: inline-block;
  margin: 4px 2px;
  transform: scale(0);
  width: 100%;
`;

export default Option;

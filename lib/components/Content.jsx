import styled from 'styled-components';

const Content = styled.div`
  height: calc(${props => props.height} - ${props => props.hideInput ? '56px' : '170px'});
  overflow-y: scroll;
  margin-top: 2px;
  padding-top: 14px;

  @media screen and (max-width: 568px) {
    height: ${props => props.floating ? 'calc(100% - 170px)' : ''};
  }
`;

export default Content;

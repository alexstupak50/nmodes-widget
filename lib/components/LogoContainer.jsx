import styled from 'styled-components';
import defaultTheme from '../theme';

const LogoContainer = styled.div`
  background: ${({ theme }) => theme.logoBackgroundColor};
  height: 61px;
  display: flex;
  align-items: center;
  padding-left: 20px;
`;

LogoContainer.defaultProps = {
  theme: defaultTheme,
};

export default LogoContainer;

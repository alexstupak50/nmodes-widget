import React from 'react';
import styled from 'styled-components';

const ActionStyledButton = styled.button`
  border: none;
  padding: 0;
  width: auto;
  overflow: visible;
  background: transparent;
  /* inherit font & color from ancestor */
  color: inherit;
  font: inherit;
  /* Normalize \`line-height\`. Cannot be changed from \`normal\` in Firefox 4+. */
  line-height: normal;
  /* Corrects font smoothing for webkit */
  -webkit-font-smoothing: inherit;
  -moz-osx-font-smoothing: inherit;
  /* Corrects inability to style clickable \`input\` types in iOS */
  -webkit-appearance: none;
  text-align: inherit;
  outline: none;

  margin: 2px;

  cursor: pointer;
  
`;

const StyledI = styled.i`
  color: #BCBCBC;
  margin: 0 2px;
  
  svg:hover {
    color: #808080;
  };
  
`;

const ActionButton = ({ onClick, children, color }) => (
  <ActionStyledButton color={color} onClick={onClick}>
    <StyledI>{ children }</StyledI>
  </ActionStyledButton>
);

export default ActionButton;


import styled from 'styled-components';

const FooterBootom = styled.div`
  display: flex;
  margin-top: 8px;
  justify-content: space-between;
`;

export default FooterBootom;

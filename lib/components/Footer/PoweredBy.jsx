import React from 'react';
import styled from 'styled-components';
import PoweredByIcon from '../../icons/PoweredByIcon';


const PoweredBy = () => (
  <React.Fragment>
    powered by <PoweredByIcon />
  </React.Fragment>
);


const StyledPoweredBy = styled.a`
  display: flex;
  align-self: flex-end;
  color: #AAAAAA;
  font-size: 11pt;
  font-family: WhitneyBook;
  justify-content: flex-end;
  
  svg {
    padding-top: 2px;
    margin-left: 5px;
  }
`;


export default () => (<StyledPoweredBy><PoweredBy /></StyledPoweredBy>);

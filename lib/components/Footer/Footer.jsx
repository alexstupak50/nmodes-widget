import styled from 'styled-components';

const Footer = styled.div`
  position: relative;
  margin: 0 20px;
  height: ${props => props.footerHeight};
`;

export default Footer;

import React from 'react';
import styled from 'styled-components';

const StyledVerticalBorder = styled.i`
  margin: 0 3px;
`;

const VerticalBorder = () => (
  <StyledVerticalBorder>
    <svg xmlns="http://www.w3.org/2000/svg" width="1" height="20.9" viewBox="0 0 1 20.9">
      <rect fill="#BCBCBC" width="1" height="20.9" />
    </svg>
  </StyledVerticalBorder>
);

export default VerticalBorder;








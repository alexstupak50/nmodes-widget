import React from 'react';
import { ThemeProvider } from 'styled-components';
import ChatBot from './index';
import CreditsContext from './context';


const defaultTheme = {
  background: '#FFFFFF',
  fontFamily: 'WhitneyMedium',
  headerBgColor: '#033745',
  headerFontColor: '#fff',
  headerFontSize: '16px',
  botBubbleColor: '#F3F5F9',
  botFontColor: '#666666',
  userBubbleColor: '#4ab8d9',
  userFontColor: '#FFFFFF',
  logoBackgroundColor: '#F3F5F9',
  buttonTextColor: '#4ab8d9',
  floatButtonColor: '#4ab8d9',
};


const blue = { ...defaultTheme};
const grey = { ...defaultTheme, headerBgColor: '#555555', userBubbleColor: '#a7d745', floatButtonColor: '#a7d745' };
const dark = { ...defaultTheme, headerBgColor: '#000000', userBubbleColor: '#666666', floatButtonColor: '#666666' };

const themes = {
  default: defaultTheme,
  blue,
  grey,
  dark,
};



const ThemedExample = ({ api_key, theme, floating}) => {
  floating = (floating !== 'false');
  const selectedTheme = themes[theme] || defaultTheme;
  return (
    <CreditsContext.Provider value={{apiKey: api_key, uploadFilesUrl: process.env.UPLOAD_FILES_URL}}>
      <ThemeProvider theme={selectedTheme} >
        <ChatBot floating={floating} apiKey={api_key} recognitionEnable={true} />
      </ThemeProvider>
    </CreditsContext.Provider>
  );
};


export default ThemedExample;

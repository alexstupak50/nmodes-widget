import React from 'react';

export function contextWrapper(Consumer, WrappedComponent) {
  return function Wrapper(props) {
    return(
      <Consumer>
        { value => (
          <WrappedComponent {...props} {...value} />
        )}
      </Consumer>
    )
  }
}

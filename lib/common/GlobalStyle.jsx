import { withTheme, injectGlobal } from 'styled-components';
import { Children } from 'react';


const wrapUrl = (url) => {
  const apiUrl = process.env.PUBLIC_URL;
  return `'${apiUrl}/fonts/${url}'`;
};

const Global = ({ theme, children }) => {

  injectGlobal`
  .nmodes_widget { 
    @font-face {
      font-family: 'WhitneyMedium';
      src: url(${wrapUrl('Whitney-Medium.eot')});
      src: url(${wrapUrl('Whitney-Medium.eot?#iefix')}) format('embedded-opentype'),
        url(${wrapUrl('Whitney-Medium.woff2')}) format('woff2'),
        url(${wrapUrl('Whitney-Medium.woff')}) format('woff'),
        url(${wrapUrl('Whitney-Medium.ttf')}) format('truetype');
      font-weight: bold;
      font-style: normal;
    }
    
    
    @font-face {
      font-family: 'WhitneyBook';
      src: url(${wrapUrl('Whitney-Book.eot')});
      src: url(${wrapUrl('Whitney-Book.eot?#iefix')}) format('embedded-opentype'),
        url(${wrapUrl('Whitney-Book.woff2')}) format('woff2'),
        url(${wrapUrl('Whitney-Book.woff')}) format('woff'),
        url(${wrapUrl('Whitney-Book.ttf')}) format('truetype');
    
      font-weight: 500;
      font-style: normal;
    }
    
    @font-face {
      font-family: 'TradeGothic';
      src: url(${wrapUrl('TradeGothic.eot')});
      src: url(${wrapUrl('TradeGothic.eot?#iefix')}) format('embedded-opentype'),
        url(${wrapUrl('TradeGothic.woff2')}) format('woff2'),
        url(${wrapUrl('TradeGothic.woff')}) format('woff'),
        url(${wrapUrl('TradeGothic.ttf')}) format('truetype');
      font-weight: 500;
      font-style: normal;
    }
    
    @font-face {
      font-family: 'TradeGothicMedium';
      src: url(${wrapUrl('TradeGothic.eot')});
      src: url(${wrapUrl('TradeGothic.eot?#iefix')}) format('embedded-opentype'),
        url(${wrapUrl('TradeGothic.woff2')}) format('woff2'),
        url(${wrapUrl('TradeGothic.woff')}) format('woff'),
        url(${wrapUrl('TradeGothic.ttf')}) format('truetype');
      font-weight: 500;
      font-style: normal;
    }
    
    @font-face {
      font-family: 'TradeGothicBold';
      src: url(${wrapUrl('TradeGothic-Bold.eot')});
      src: url(${wrapUrl('TradeGothic-Bold.eot?#iefix')}) format('embedded-opentype'),
        url(${wrapUrl('TradeGothic-Bold.woff2')}) format('woff2'),
        url(${wrapUrl('TradeGothic-Bold.woff')}) format('woff'),
        url(${wrapUrl('TradeGothic-Bold.ttf')}) format('truetype');
      font-weight: 500;
      font-style: normal;
    }
    
    @font-face {
      font-family: 'TradeGothicBoldTwo';
      src: url(${wrapUrl('TradeGothic-BoldTwo.eot')});
      src: url(${wrapUrl('TradeGothic-BoldTwo.eot?#iefix')}) format('embedded-opentype'),
        url(${wrapUrl('TradeGothic-BoldTwo.woff2')}) format('woff2'),
        url(${wrapUrl('TradeGothic-BoldTwo.woff')}) format('woff'),
        url(${wrapUrl('TradeGothic-BoldTwo.ttf')}) format('truetype');
      font-weight: 500;
      font-style: normal;
    }
    
    
    @font-face {
      font-family: 'WhitneyBold';
    
      src: url(${wrapUrl('Whitney-Bold.eot')});
      src: url(${wrapUrl('Whitney-Bold.eot?#iefix')}) format('embedded-opentype'),
        url(${wrapUrl('Whitney-Bold.woff2')}) format('woff2'),
        url(${wrapUrl('Whitney-Bold.woff')}) format('woff'),
        url(${wrapUrl('Whitney-Bold.ttf')}) format('truetype');
      font-weight: 500;
      font-style: normal;
    }
  }
`;
  return Children.only(children);

};

export default withTheme(Global);
